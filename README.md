Congratulations! You've made it to the project stage of our interview process.

Please create a new branch and complete the challenges you'll find under the code_review and pattern_matching directories. When you've finished, please submit a merge request.

If you have any questions or require more time to complete these challenges, don't hesitate to reach out.
