// Insert this component directly into another React component or a Ruby erb to display a link that holds and triggers a the edit access/team modal
// See propTypes listed below for required and optional props to pass in

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'react-bootstrap';

import TeamModal from './TeamModal.jsx';

class TeamModalLink extends Component {
	constructor(props) {
		super(props);

		this.state = {
			showModal: false,

		}
	}

	toggleModal = () => {
		this.setState({
			showModal: !this.state.showModal,
		});
	}

	render () {
		return (
			<span>
				<Button
				  aria-label="Open comments"
				  onClick={() => this.setState({ showModal: !this.state.showModal })}
				  bsStyle="link"
				>
				  Edit access
				</Button>

				{this.state.showModal ?
					<Modal
					  show={this.state.showModal}
					  onHide={() => this.setState({ showModal: false })}
					  bsSize="large"
					  backdropClassName="dark-modal-backdrop"
					  dialogClassName="bridge-modal"
					>
					  <TeamModal
						  type={this.props.objectType}
						  objectName={this.props.objectName}
						  objectId={this.props.objectId}
						  owner={this.props.owner}
						  ownerId={this.props.owner.id}
						  currentUserRole={this.props.currentUserRole}
						  updateCallback={this.updateOwner}
						  updateUsersCallback={this.props.updateUsersCallback}
						  matterSlug={this.props.matterSlug}
					  />
					</Modal>
				: null}
			</span>
		);
	}
}

TeamModalLink.propTypes = {
	objectId: PropTypes.number.isRequired,
	objectType: PropTypes.string.isRequired,
	updateCallback: PropTypes.func,
	updateUsersCallback: PropTypes.func,
	objectName: PropTypes.string,
	owner: PropTypes.object,
	ownerId: PropTypes.number,
	matterSlug: PropTypes.string,
	currentUserRole: PropTypes.string,
	type: PropTypes.string,
}

TeamModalLink.defaultProps = {
	updateUsersCallback: null,
	owner: {},
	ownerId: null,
	matterSlug: '',
	currentUserRole: 'user',
	type: 'Task',
}

export default TeamModalLink;
