Congratulations! You've made it to the project stage of our interview process.

Please fork this repository and complete the challenges you'll find under the code_review and pattern_matching branches. There will be additional instructions in each branches README.

If you have any questions or require more time to complete these challenges, don't hesitate to reach out.